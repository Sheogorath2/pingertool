# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import pinger, json
from urllib.parse import parse_qs


def makeresponse(output, start_response):
    status = '200 OK'
    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(str(output))))]
    start_response(status, response_headers)


def application(environ, start_response):
    parameters = parse_qs(environ.get('QUERY_STRING', ''))
    if ('hosts' in parameters):
        hosts = str(parameters['hosts'][0]).replace(';', ',').split(',')
    else:
        rp = "Use 'hosts' parameter for input"
        makeresponse(rp, start_response)
        return (rp)

    pinged = pinger.ping(hosts)
    print(pinged)
    print("                ")
    uniquedict = {}
    for state in pinged:
        unique = []
        [unique.append(item) for item in pinged[state] if item not in unique]
        if (state == "alive"): uniquedict['alive'] = unique
        if (state == "dead"): uniquedict['dead'] = unique
    output = json.dumps(uniquedict)
    print(str(output))
    makeresponse(output, start_response)
    return str(output)
